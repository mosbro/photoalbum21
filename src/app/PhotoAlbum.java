package app;
	
import java.io.File;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Photo Album JavaFX application. It can add albums which hold photos
 * for individual users. All albums stored in the data folder.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class PhotoAlbum extends Application 
{
	@Override
	public void start(Stage primaryStage) 
	{
		// Do any necessary startup operations
		new File("data/users").mkdirs();
		StateManager.getInstance().setStage(primaryStage);
			
		// Load the Login screen
		StateManager.getInstance().loadScene("Login.fxml", null);
	}
	
	/**
	 * Launches the JavaFX GUI application.
	 * 
	 * @param args Command line arguments.
	 */
	public static void main(String[] args) 
	{
		launch(args);
	}
}
