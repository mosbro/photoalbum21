package app;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import photoalbum.UserList;
import view.Controller;

/**
 * ScreenManager is a singleton that loads other screens in the program.
 * It takes the FXML file and any arguments that need to be passed to
 * other controllers. 
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class StateManager 
{
	/** The actual instance of the ScreenManager to make it a singleton. */
	private static StateManager instance;
	
	/** User list that holds all the users. */
	private UserList userList;
	
	/** The stage that will be switching screens through FXMLs. */
	private Stage stage;
	
	/**
	 * Default constructor of ScreenManager.
	 */
	private StateManager()	
	{
		userList = new UserList();
	}
	
	/**
	 * Gets the ScreenManager instance.
	 * 
	 * @return ScreenManager object.
	 */
	public static StateManager getInstance()
	{
		if(instance == null)
			instance = new StateManager();
		
		return instance;
	}
	
	/**
	 * Gets all the users registered.
	 * 
	 * @return A UserList object.
	 */
	public UserList getUsers() { return this.userList; }
	
	/**
	 * Sets the stage where the screens will be changing.
	 * 
	 * @param stage The stage to be altered.
	 */
	public void setStage(Stage stage) { this.stage = stage; }
	
	/**
	 * Get the current stage.
	 * 
	 * @return A stage object.
	 */
	public Stage getStage() { return this.stage; }
	
	/**
	 * Loads a new scene given the FXML file name and passes the 
	 * argument over to the correct controller.
	 * 
	 * @param fxml The FXML file name with the extension.
	 * @param arg The argument to be passed to the Controller.
	 */
	public void loadScene(String fxml, Object[] arg)
	{	
		// Close the previous stage
		this.stage.close();
		
		try
		{
			// Load the scene given
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/" + fxml));
			
			// Setup new stage
			Stage newStage = new Stage();
			newStage.setScene(new Scene((AnchorPane) loader.load(), 790, 590));
			newStage.setResizable(false);
			newStage.setTitle("Photo Album");
			newStage.getIcons().add(new Image(getClass().getResourceAsStream("/icon.png")));
			
			// Show the stage and set current stage
			newStage.show();
			
			// Load the controller and start it
			((Controller) loader.getController()).start(arg);
			
			// Close previous stage and set new one
			this.stage = newStage;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
