package photoalbum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Album class holds all the photos for a particular album.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class Album implements Serializable
{
	/** Serial version unique identification. */
	private static final long serialVersionUID = 1L;
	
	/** Name of them album. */
	private String albumName;
	
	/** Number of photos in the album. */
	private String photoCount;
	
	/** The date of the oldest photo. */
	private String dateOld;
	
	/** List of photos for an album. */
	private ArrayList<Photo> photos;
	
	/**
	 * Constructor for album which takes an album name. 
	 * 
	 * @param name The name of the album.
	 */
	public Album(String name)
	{
		this.albumName = name;
		this.photos = new ArrayList<Photo>();
		this.photoCount = "0";
		this.dateOld = "-";
	}
	
	/**
	 * Gets the name of album.
	 * 
	 * @return String as the album name.
	 */
	public String getAlbumName() { return this.albumName; }
	
	/**
	 * Sets the name of album.
	 * 
	 * @param name String as the album name.
	 */
	public void setAlbumName(String name) { this.albumName = name; }
	
	/**
	 * Get the date range from earliest to latest of photos.
	 * 
	 * @return String holding the date range in the format MM/dd/yyyy
	 */
	public String getDateRange() { return this.getDateOld() + " - " + this.getDateNew(); }
	
	/**
	 * Get the date of the oldest photo.
	 * 
	 * @return String holding the date of the oldest photo.
	 */
	public String getDateOld() 
	{ 
		if(this.photos.size() != 0)
		{
			// Get first date
			Calendar date = this.photos.get(0).getDate();
			int index = 0;
			
			for(index = 0; index < this.photos.size(); index++)
			{
				if(date.compareTo(this.photos.get(index).getDate()) < 0)
					date = this.photos.get(index).getDate();
			}
			
			// Convert to string
			index--;
			int month = this.photos.get(index).getDate().get(Calendar.MONTH);
			int day = this.photos.get(index).getDate().get(Calendar.DAY_OF_MONTH);
			int year = this.photos.get(index).getDate().get(Calendar.YEAR);
			this.dateOld = month + "/" + day + "/" + year;
		}
		
		// Return it
		return this.dateOld; 
	}
	
	/**
	 *  Get the date of the photo whose modification date is earliest.
	 * 
	 * @return String holding the earliest modified date.
	 */
	public String getDateNew() 
	{ 
		// Store new date
		String dateNew = "";
		
		if(this.photos.size() != 0)
		{
			// Get first date
			Calendar date = this.photos.get(0).getDate();
			int index = 0;
			
			for(index = 0; index < this.photos.size(); index++)
			{
				if(date.compareTo(this.photos.get(index).getDate()) > 0)
					date = this.photos.get(index).getDate();
			}
			
			// Convert to string
			index--;
			int month = this.photos.get(index).getDate().get(Calendar.MONTH);
			int day = this.photos.get(index).getDate().get(Calendar.DAY_OF_MONTH);
			int year = this.photos.get(index).getDate().get(Calendar.YEAR);
			dateNew = month + "/" + day + "/" + year;
		}
		
		// Return it
		return dateNew; 
	}
	
	/**
	 * Gets the list of photos.
	 * 
	 * @return ArrayList of photos.
	 */
	public ArrayList<Photo> getPhotoList() { return this.photos; }
	
	/**
	 * Gets the number of photos in the list.
	 * 
	 * @return An integer with the size of the list.
	 */
	public int getPhotoListSize() { return this.photos.size(); }
	
	/** 
	 * Gets the number of photos in the list as a String.
	 * 
	 * @return A string with the number of photos in the album.
	 */
	public String getPhotoCount() { return this.photoCount; }
	
	/**
	 * Gets the photo in the list at the specified index.
	 * 
	 * @param index Location of the photo in the list.
	 * @return Photo inside the Album.
	 */
	public Photo getPhoto(int index) { return this.photos.get(index); }
	
	/**
	 * Adds a photo to the album.
	 * 
	 * @param photo The photo to be added.
	 */
	public void addPhoto(Photo photo) 
	{ 
		this.photos.add(photo);
		photoCount = Integer.toString(this.photos.size());
	}
	
	/**
	 * Remove a photo from the album.
	 * 
	 * @param index The location of the photo in the list.
	 */
	public void removePhoto(int index) 
	{ 
		this.photos.remove(index);
		photoCount = Integer.toString(this.photos.size());
	}
	
	/**
	 * Get the string representation of this object.
	 */
	public String toString() { return this.albumName; }
}
