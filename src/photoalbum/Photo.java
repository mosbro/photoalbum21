package photoalbum;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Photo class implements a single photo. Holds the physical location
 * of the photo on disk and the date modified in the program.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class Photo implements Serializable
{
	/** Serial version unique identification. */
	private static final long serialVersionUID = 1L;
	
	/** Name of the photo. */
	private String caption;
	
	/** File path of the photo on disk, */
	private String location;
	
	/** The last date the photo was modified inside the program. */
	private Calendar date;
	
	/** List of tags that have type and value. */
	private ArrayList<Tag> tags;
	
	/**
	 * Photo constructor. Takes the location on disk.
	 * 
	 * @param location File path of the photo on disk.
	 */
	public Photo(String location)
	{
		this.caption = "   No caption";
		this.location = location;
		tags = new ArrayList<Tag>();
		this.date = Calendar.getInstance();
		this.setDate();
	}
	
	/**
	 * Photo constructor. Takes a name and the location on disk.
	 * 
	 * @param caption Name of the photo.
	 * @param location File path of the photo on disk.
	 */
	public Photo(String caption, String location)
	{
		this.caption = caption;
		this.location = location;
		tags = new ArrayList<Tag>();
		this.date = Calendar.getInstance();
		this.setDate();
	}
	
	/**
	 * Add a tag to the photo.
	 * 
	 * @param tag Tag object with a type and value.
	 * @return True or false depending on if the tag was added.
	 */
	public boolean addTag(Tag tag)
	{
		if(checkTag(tag))
		{
			return false;
		}
		
		tags.add(tag);
		return true;
	}
	
	/**
	 * Checks to see if the tag is in the list of tags.
	 * 
	 * @param tag The tag to look for.
	 * @return True or false depending on if the same tag was found.
	 */
	public boolean checkTag(Tag tag)
	{
		String tagType = tag.getType();
		String tagValue = tag.getValue();
		
		for(Tag var: this.tags)
		{
			if(var.getType().equalsIgnoreCase(tagType) && var.getValue().equalsIgnoreCase(tagValue))
				return true;
		}
		
		return false;
	}
	
	/**
	 * Remove a tag from the list.
	 * 
	 * @param index The position of the tag in the list.
	 */
	public void removeTag(int index) { this.tags.remove(index); }
	
	/**
	 * Get a tag from the list.
	 * 
	 * @param index The position of the tag in the list.
	 * @return A tag object holding the type and value.
	 */
	public Tag getTag(int index) { return this.tags.get(index); }
	
	/**
	 * Get the list of tags the photo holds.
	 * 
	 * @return An array list of tags.
	 */
	public ArrayList<Tag> getTags() { return this.tags; }
	
	/**
	 * Get the size of the tag list.
	 * 
	 * @return An integer holding the size.
	 */
	public int getTagListSize() { return this.tags.size(); }
	
	/**
	 * Gets the caption of the photo.
	 * 
	 * @return A string holding the caption.
	 */
	public String getCaption() { return this.caption; }
	
	/**
	 * Sets the caption of the photo.
	 * 
	 * @param caption The caption to add to the photo.
	 */
	public void setCaption(String caption) { this.caption = caption; }
	
	/**
	 * Gets the physical location of the photo on disk.
	 * 
	 * @return A string holding the file path of the photo.
	 */
	public String getLocation() { return this.location; }
	
	/**
	 * Sets the location of the photo on the disk.
	 * 
	 * @param location The file path of the photo.
	 */
	public void setLocation(String location) { this.location = location; }
	
	/**
	 * Gets the date the photo was last modified inside the album.
	 * 
	 * @return A Calendar object holding the date.
	 */
	public Calendar getDate() { return this.date; }
	
	/**
	 * Sets the date of the photo.
	 */
	public void setDate() 
	{ 
		File file = new File(this.location.substring(5));
		this.date.setTimeInMillis(file.lastModified());
		this.date.set(Calendar.MILLISECOND, 0);
	}
	
	/**
	 * Sees if the photo date is between the two dates.
	 * 
	 * @param dateOne A Calendar object holding the start date.
	 * @param dateTwo A Calendar object holding the end date.
	 * @return True if between, false if not.
	 */
	public boolean isInRange(Calendar dateOne, Calendar dateTwo) 
	{
		return (this.date.before(dateTwo) && this.date.after(dateOne));
	}
}
