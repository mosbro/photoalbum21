package photoalbum;

import java.io.Serializable;

/**
 * Tag class holds a tag type and a tag value
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class Tag implements Serializable
{
	/** Serial version unique identification. */
	private static final long serialVersionUID = 1L;
	
	/** The type of tag (ex. location). */
	private String type;
	
	/** The value of tag (ex. New Brunswick). */
	private String value;
	
	/**
	 * Tag constructor that takes a type and a value.
	 * 
	 * @param type The type of tag (ex. location).
	 * @param value The value of tag (ex. New Brunswick).
	 */
	public Tag(String type, String value)
	{
		this.type = type;
		this.value = value;
	}
	
	/**
	 * Sets the tag type (ex. location).
	 * 
	 * @param type String with the type.
	 */
	public void setType(String type) { this.type = type; }
	
	/**
	 * Sets the tag value (ex. New Brunswick).
	 * 
	 * @param value String with the value.
	 */
	public void setValue(String value) { this.type = value; }
	
	/**
	 * Gets the tag type.
	 * 
	 * @return String holding the type.
	 */
	public String getType() { return this.type; }
	
	/**
	 * Gets the tag value.
	 * 
	 * @return String holding the value.
	 */
	public String getValue() { return this.value; }
	
	/**
	 * String representation of the tag.
	 */
	public String toString() { return this.type + ": " + this.value; }
}
