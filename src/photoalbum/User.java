package photoalbum;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * User class holds a list of albums and the login credentials.
 * A user is able to serialize itself with the file name "username.dat".
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class User implements Serializable
{
	/** Serial version unique identification. */
	private static final long serialVersionUID = 1L;
	
	/** Unique name of a User. */
	private String username;
	
	/** Password of the User for login. */
	private String password;
	
	/** The albums that the User has. */
	private ArrayList<Album> albums;
	
	/**
	 * User constructor which takes the username and password.
	 * 
	 * @param username Unique name of a user.
	 * @param password Password for the user.
	 */
	public User(String username, String password)
	{
		this.username = username;
		this.password = password;
		albums = new ArrayList<Album>();
	}
	
	/**
	 * Gets the username for a user.
	 * 
	 * @return A String holding the username.
	 */
	public String getUsername() { return this.username; }
	
	/**
	 * Gets the password for a user.
	 * 
	 * @return A String holding the password.
	 */
	public String getPassword() { return this.password; }
	
	/**
	 * Add an album for the user.
	 * 
	 * @param album An album object.
	 */
	public void addAlbum(Album album) { this.albums.add(album); }
	
	/**
	 * Get an album from the list given the index.
	 * 
	 * @param index The location in the album list.
	 * @return An album object.
	 */
	public Album getAlbum(int index) { return this.albums.get(index); }
	
	/**
	 * Removes an album from the user.
	 * 
	 * @param index The position of the album in the list.
	 */
	public void removeAlbum(int index) { this.albums.remove(index); }
	
	/**
	 * Gets the number of albums in the list.
	 * 
	 * @return An integer with the size of the list.
	 */
	public int getAlbumListSize() { return this.albums.size(); }
	
	/**
	 * Gets the list of albums.
	 * 
	 * @return An arraylist holding the albums.
	 */
	public ArrayList<Album> getAlbumList() { return this.albums; }
	
	/**
	 * Checks the username and password and see if it matches the given values.
	 * 
	 * @param username The username from the text field.
	 * @param password The password from the text field.
	 * 
	 * @return True or false depending on the match.
	 */
	public boolean checkInfo(String username, String password)
	{
		// Check if username (case ignored) and password match.
		if(username.equalsIgnoreCase(this.username) && password.equals(this.password))
			return true;
		
		// No match
		return false;
	}
	
	/**
	 * Look for an album with the given name.
	 * 
	 * @param name The album name to find.
	 * @return The album that corresponds to the name or null.
	 */
	public Album findAlbum(String name)
	{
		for(Album album : this.albums)
		{
			if(album.getAlbumName().equalsIgnoreCase(name))
				return album;
		}
		
		return null;
	}
	
	/**
	 * Finds the index of an album.
	 * 
	 * @param name The album name to look for.
	 * @return An integer representing the index.
	 */
	public int findAlbumIndex(String name)
	{
		for(int i = 0; i < this.albums.size(); i++)
		{
			if(this.albums.get(i).getAlbumName().equalsIgnoreCase(name))
				return i;
		}
		
		return 0;
	}
	
	/**
	 * Serializes the User into a file called "username.dat".
	 */
	public void serialize()
	{
		// Location and name of the user file
		String userFile = "data/users/" + this.username + ".dat";
		
		try
		{
			FileOutputStream fileOut = new FileOutputStream(userFile);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(this);
			out.close();
			fileOut.close();
	         
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * String representation of the user.
	 */
	public String toString() { return this.getUsername(); }
}
