package photoalbum;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class UserList
{
	/** ObservableList which holds Users and their photo albums. */
	private ObservableList<User> userList = FXCollections.observableArrayList();
	
	/**
	 * Default constructor of the user list. Deserializes the users and populates the list.
	 */
	public UserList()
	{
		// Populate the list
		this.populate();
		
		// Test data
		if(this.findUser("mo") == null && this.findUser("luke") == null)
		{
			User mo = new User("mo", "oo");
			User luke = new User("luke", "oo");
			this.userList.add(mo);
			this.userList.add(luke);
			
			// Add test data
			this.addTestData("data/albums/mo/nature", "Nature", "mo", 0);
			this.addTestData("data/albums/mo/art", "Art", "mo", 1);
			this.addTestData("data/albums/luke", "Random Photos", "luke", 0);
			
			mo.getAlbum(0).getPhoto(0).setCaption("   Hopetun Falls");
			mo.getAlbum(0).getPhoto(1).setCaption("   Railway");
			mo.getAlbum(0).getPhoto(2).setCaption("   Purple Fields");
			mo.getAlbum(0).getPhoto(3).setCaption("   Quiet Lake");
			mo.getAlbum(0).getPhoto(4).setCaption("   Random Waterfall");
			mo.getAlbum(0).getPhoto(0).addTag(new Tag("Place", "Hopetun"));
			mo.getAlbum(0).getPhoto(1).addTag(new Tag("Place", "Colorado"));
			mo.getAlbum(0).getPhoto(2).addTag(new Tag("Color", "Purple"));
			mo.getAlbum(0).getPhoto(4).addTag(new Tag("Type", "Waterfall"));
			mo.getAlbum(1).getPhoto(0).setCaption("   DJ Music");
			mo.getAlbum(1).getPhoto(0).addTag(new Tag("Color", "Purple"));
			
			luke.getAlbum(0).getPhoto(0).setCaption("   Close");
			luke.getAlbum(0).getPhoto(1).setCaption("   Copy");
			luke.getAlbum(0).getPhoto(2).setCaption("   Edit");
			luke.getAlbum(0).getPhoto(0).addTag(new Tag("Icon", "Close"));
			luke.getAlbum(0).getPhoto(0).addTag(new Tag("Color", "Red"));
			luke.getAlbum(0).getPhoto(1).addTag(new Tag("Icon", "Folder"));
			
			// Serialize
			mo.serialize();
			this.setUser(mo);
			
			// Serialize
			luke.serialize();
			this.setUser(luke);
		}
	}
	
	/**
	 * Gets the list of users.
	 * 
	 * @return An ObservableList allocated with Users.
	 */
	public ObservableList<User> getList() { return userList; }
	
	/**
	 * Adds a User to the list.
	 * 
	 * @param user The user being added to the list.
	 */
	public void addUser(User user) 
	{ 
		userList.add(user);
		user.serialize();
	}
	
	/**
	 * Sets the user where the username is the same in the list.
	 * 
	 * @param user The user to be set.
	 */
	public void setUser(User user)
	{
		for(int i = 0; i < userList.size(); i++)
		{
			if(userList.get(i).getUsername().equalsIgnoreCase(user.getUsername()))
			{
				userList.set(i, user);
			}
		}
	}
	
	/**
	 * Gets a User from the ObservableList.
	 * 
	 * @param index Position of the User in the list.
	 * @return The specified User.
	 */
	public User getUser(int index) { return userList.get(index); }
	
	/**
	 * Finds a user in the list given a username.
	 * 
	 * @param username The name of the user.
	 * @return A User object.
	 */
	public User findUser(String username)
	{
		for(User user : userList)
		{
			if(user.getUsername().equalsIgnoreCase(username))
				return user;
		}
		
		return null;
	}
	
	/**
	 * Finds a user in the list given a username and removes them.
	 * 
	 * @param username The name of the user.
	 * @return A User object.
	 */
	public boolean removeUser(String username)
	{
		for(int i = 0; i < userList.size(); i++)
		{
			if(userList.get(i).getUsername().equalsIgnoreCase(username))
			{
				new File("data/users/" + userList.get(i).getUsername() + ".dat").delete();
				userList.remove(i);
				return true;
			}
		}
		
		// User not removed
		return false;
	}
	
	/**
	 * Reads all the user files (by deserializing them) and adds each user to the list. 
	 * Will look in the current directory for the folder "data/users".
	 */
	public void populate()
	{
		// Add the admin user
		userList.add(new User("admin", "admin"));
		
		// Get the user files
		String userDir = "data/users/";
		List<String> userFiles = getUserFiles(userDir);
		
		// Deserialize and add users to list
		try 
		{
			// Loop through the files
			for(int i = 0; i < userFiles.size(); i++)
			{
				// Get the correct user
				FileInputStream fileIn = new FileInputStream(userDir + userFiles.get(i));
				ObjectInputStream in = new ObjectInputStream(fileIn);
				
				// Add user to the list
				userList.add((User) in.readObject());
				
				// Close the streams
				in.close();
				fileIn.close();
			}
			
		} catch(IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void addTestData(String path, String albumName, String username, int index)
	{
		// Get test data
		List<String> userFiles = getUserFilesWithPath(path);
		
		for(User user : this.userList)
		{
			if(user.getUsername().equals(username))
			{
				user.addAlbum(new Album(albumName));
				
				for(int i = 0; i < userFiles.size(); i++)
				{
					user.getAlbum(index).addPhoto(new Photo("file:" + userFiles.get(i)));
				}
				
				// Serialize
				user.serialize();
				this.setUser(user);
			}
		}
	}
	
	/**
	 * Goes in the folder "data/users" and gets all the files (no directories).
	 * Each entry is added to a List with only their names and extension.
	 * 
	 * @return A List of usernames as Strings.
	 */
	private List<String> getUserFiles(String path)
	{
		// Get the user files from the users folder
		List<String> userFiles = new ArrayList<String>();
		File[] listOfUsers = new File(path).listFiles();
		
		// Loop through the list and get only the files (no directories)
		for(File file : listOfUsers)
		{
			if(file.isFile())
				userFiles.add(file.getName());
		}
		
		return userFiles;
	}
	
	/**
	 * Goes in the folder "data/albums/user" and gets all the files (no directories).
	 * Each entry is added to a List with paths.
	 * 
	 * @return A List of photos with their paths.
	 */
	private List<String> getUserFilesWithPath(String path)
	{
		// Get the user files from the users folder
		List<String> userFiles = new ArrayList<String>();
		File[] listOfUsers = new File(path).listFiles();
		
		// Loop through the list and get only the files (no directories)
		for(File file : listOfUsers)
		{
			if(file.isFile())
				userFiles.add(file.getAbsolutePath());
		}
		
		return userFiles;
	}
}
