package view;

import java.io.IOException;

import app.StateManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import photoalbum.User;

/**
 * AdminController allows the admin to create and delete users.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class AdminController implements Controller
{
	/** The menu bar control with all the actions. */
	@FXML private MenuController menuController;
	
	/** Button that performs operation on the text field value. */
	@FXML private Button addUserBtn, deleteUserBtn;
	
	/** List which displays all the information on each album. */
	@FXML private ListView<User> userListView;
	
	/** Label which displays information on the user or album. */
	@FXML private Label userLabel, passwordLabel, warningLabel;
	
	/** TextField that holds the username. */
	@FXML private TextField userField, passwordField;
	
	
	/** {@inheritDoc} */
	public void start(Object[] obj) 
	{
		// Menu stuff
		this.menuController.start(null);
		menuController.setAdminVisibility();
		
		// Add items to list
		userListView.setItems(StateManager.getInstance().getUsers().getList());
		
		// Button Events
		addUserBtn.setOnAction(e -> addUser());
		deleteUserBtn.setOnAction(e -> confirm());
		
		// List view listener
		userListView.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) -> updateLabels());
	}
	
	/**
	 * Adds a user to the master user list.
	 */
	public void addUser()
	{
		// Get the username and password text values
		String username = userField.getText();
		String password = passwordField.getText();
		
		if(!username.equals("") && !password.equals(""))
		{
			// Add new user
			StateManager.getInstance().getUsers().addUser(new User(username, password));
			
			// Success label
			this.warningLabel.setText("User added successfully!");
			this.warningLabel.setVisible(true);
		}
		else
		{
			this.warningLabel.setText("Please fill in both fields");
			this.warningLabel.setVisible(true);
		}
	}
	
	/**
	 * Confirm if a user should be deleted.
	 */
	public void confirm()
	{	
		try
		{
			// Load the scene given
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Dialog.fxml"));
			
			// Setup new stage
			Stage newStage = new Stage();
			newStage.setScene(new Scene((AnchorPane) loader.load()));
			newStage.setResizable(false);
			
			// Load the controller and start it
			((Controller) loader.getController()).start(new Object[] {newStage});
			
			// Show the stage and set current stage
			newStage.showAndWait();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(DialogController.getValue() == true)
		{
			StateManager.getInstance().getUsers().removeUser(userListView.getSelectionModel().getSelectedItem().getUsername());
		}
	}
	
	/**
	 * Update the labels with the appropriate message.
	 */
	private void updateLabels()
	{
		userLabel.setText(userListView.getSelectionModel().getSelectedItem().toString());
		passwordLabel.setText(userListView.getSelectionModel().getSelectedItem().getPassword());	
	}
}
