package view;

import java.io.IOException;

import app.StateManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import photoalbum.Album;
import photoalbum.User;

/**
 * AlbumController implements the operations on the photo album view.
 * The album name, number of photos in that album, date of oldest
 * photo, and date range in the album are displayed. A user can create
 * an empty album, rename an album, or delete an album.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class AlbumController implements Controller
{
	/** The menu bar control with all the actions. */
	@FXML private MenuController menuController;
	
	/** Label which displays information on the user or album. */
	@FXML private Label welcomeLabel, subtextLabel, editLabel, successLabel;
	
	/** Image on the album view screen. */
	@FXML private ImageView albumImage, editImage, successImage;
	
	/** TextField where you can name or rename an album. */
	@FXML private TextField albumTextField;
	
	/** Button that performs operation on the text field value. */
	@FXML private Button submitButton, cancelButton, openButton;
	
	/** Table which displays all the information on each album. */
	@FXML private TableView<Album> albumTable;
	
	/** Column in the table view with information about the album. */
	@FXML private TableColumn<Album, String> albumColumn, countColumn, dateOldColumn, dateRangeColumn;
	
	/** Observable List that holds albums for the table. */
	private ObservableList<Album> albumList;
	
	/** Boolean to know which menu was pressed. */
	private boolean isCreate;
	
	/** Alert dialog box to show error messages. */
	private Alert errorAlert;
	
	/** {@inheritDoc} */
	public void start(Object[] obj) 
	{
		// Get the user
		User user =  StateManager.getInstance().getUsers().findUser(((User) obj[0]).getUsername());
		
		// Necessary operations
		this.albumList = FXCollections.observableArrayList();		// Make a observable array list
		this.menuController.start(new Object[] {user});				// Setup the menu
		this.menuController.setAlbumVisibility();					// Disable certain menu items
		this.setupTable(user);										// Setup the table to get all the albums
		this.errorAlert = new Alert(AlertType.ERROR);				// Setup the alert dialog box
		this.isCreate = false;										// Boolean for create menu item
		
		// Get username and set welcome message
		String username = Character.toUpperCase(user.getUsername().charAt(0)) + user.getUsername().substring(1);
		this.welcomeLabel.setText("Welcome " + username + "!");
		
		// Show create if no albums
		if(this.albumList.size() == 0)
			this.createAlbum();
		
		// Button & Menu events
		this.openButton.setOnAction(e -> openAlbum(user));
		this.submitButton.setOnAction(e -> submit(user));
		this.cancelButton.setOnAction(e -> cancel());
		this.menuController.getMenuItem("createAlbum").setOnAction(e -> createAlbum());
		this.menuController.getMenuItem("renameAlbum").setOnAction(e -> renameAlbum());
		this.menuController.getMenuItem("deleteAlbum").setOnAction(e -> deleteAlbum(user));

		// Add table listener
		this.albumTable.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) -> changeTableDetails());
		this.albumTable.getSelectionModel().select(0);
	}
	
	/**
	 * Opens the selected album up to view the photos inside.
	 */
	private void openAlbum(User user)
	{
		// Get selected index
		int index = albumTable.getSelectionModel().getSelectedIndex();
		
		// Load the photo view
		StateManager.getInstance().loadScene("Photo.fxml", new Object[] {user, index});
	}
	
	/**
	 * Create a new album with the name from the text field.
	 */
	private void createAlbum()
	{
		// Set create to true for submit and show form
		this.isCreate = true;
		this.setVisibility(true);
		this.successImage.setVisible(false);
		this.successLabel.setVisible(false);
		this.albumTextField.requestFocus();
		
		// Set the correct text
		this.editLabel.setText("Create");
		
		// Disable the cancel button if size == 0
		if(this.albumList.size() == 0)
			this.cancelButton.setDisable(true);
		else
			this.cancelButton.setDisable(false);
	}
	
	/**
	 * Rename the album selected on the table with the value from the text field.
	 */
	private void renameAlbum()
	{
		if(this.albumList.size() != 0)
		{
			// Set create to true for submit and show form
			this.isCreate = false;
			this.setVisibility(true);
			this.cancelButton.setDisable(false);
			this.successImage.setVisible(false);
			this.successLabel.setVisible(false);
			this.albumTextField.requestFocus();
			
			// Fill text field
			int index = albumTable.getSelectionModel().getSelectedIndex();
			this.albumTextField.setText(this.albumList.get(index).getAlbumName());
			
			// Set the correct text
			this.editLabel.setText("Rename");
		}
	}
	
	/**
	 * Delete the album selected on the table.
	 */
	private void deleteAlbum(User user)
	{
		// Get selected index
		int index = albumTable.getSelectionModel().getSelectedIndex();
		
		try
		{
			// Load the dialog scene
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Dialog.fxml"));
			
			// Setup new stage
			Stage newStage = new Stage();
			newStage.setScene(new Scene((AnchorPane) loader.load()));
			newStage.setResizable(false);
			
			// Load the controller and start it
			((Controller) loader.getController()).start(new Object[] {newStage});
			
			// Show the stage and wait until it's done
			newStage.showAndWait();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(DialogController.getValue() == true)
		{
			// Remove album
			user.removeAlbum(index);
			this.albumList.remove(index);
			
			// Select
			this.albumTable.getSelectionModel().select(index);
			
			// Serialize the user
			user.serialize();
			StateManager.getInstance().getUsers().setUser(user);
		}
	}
	
	/**
	 * Submit either creates or renames an album.
	 * 
	 * @param user The user where the album should be created or renamed.
	 */
	private void submit(User user)
	{
		// Get album name
		String albumName = this.albumTextField.getText();
		
		// Get index of album in list and hold valid boolean
		int index = albumTable.getSelectionModel().getSelectedIndex();
		boolean isValid = false;
		String successMessage = "";
		
		// Check if create or rename
		if(isCreate && !albumName.equals(""))
		{
			// Check if album with same name exists
			if(user.findAlbum(this.albumTextField.getText()) == null)
			{
				// Create new album and add to both lists
				Album newAlbum = new Album(albumName);
				user.addAlbum(newAlbum);
				albumList.add(newAlbum);
				
				// Success
				index = albumList.size() - 1;
				successMessage = "Added album!";
				isValid = true;
			}
			else
				this.showCustomAlert("Album already exists with that name!");
		}
		else if(!isCreate && !albumName.equals(""))
		{
			// Set new name
			user.findAlbum(albumTable.getSelectionModel().getSelectedItem().getAlbumName()).setAlbumName(albumName);			
			albumList.set(index, user.findAlbum(albumTable.getSelectionModel().getSelectedItem().getAlbumName()));
			
			// Success
			successMessage = "Renamed album!";
			isValid = true;
		}
		else
			this.showCustomAlert("Text field cannot be left blank!");
		
		// Serialize if valid
		if(isValid)
		{
			// Serialize the user
			user.serialize();
			StateManager.getInstance().getUsers().setUser(user);
			
			// Show successful message
			this.successImage.setVisible(true);
			this.successLabel.setVisible(true);
			this.successLabel.setText(successMessage);

			// Close the form and select album
			this.albumTable.getSelectionModel().select(index);
			this.setVisibility(false);
		}
	}
	
	/**
	 * Cancel the create or rename operation.
	 */
	private void cancel()
	{
		// Close the form
		this.setVisibility(false);
	}
	
	/** 
	 * Change the information on the screen depending on which album is selected.
	 * 
	 */
	private void changeTableDetails()
	{
		// Get selected index
		int index = albumTable.getSelectionModel().getSelectedIndex();
		
		if(albumList.size() != 0 && index >= 0 && index < this.albumList.size())
			this.albumTextField.setText(this.albumList.get(index).getAlbumName());
		else
			this.createAlbum();
	}
	
	/**
	 * Sets up the table with the albums from the user.
	 * 
	 * @param user The user holding the albums.
	 */
	private void setupTable(User user)
	{
		// Add the albums to observable list from the user
		for(int i = 0; i < user.getAlbumListSize(); i++)
		{
			this.albumList.add(user.getAlbum(i));
		}
		
		// Add the columns
		this.albumColumn.setCellValueFactory(new PropertyValueFactory<Album, String>("albumName"));
		this.countColumn.setCellValueFactory(new PropertyValueFactory<Album, String>("photoCount"));
		this.dateOldColumn.setCellValueFactory(new PropertyValueFactory<Album, String>("dateOld"));
		this.dateRangeColumn.setCellValueFactory(new PropertyValueFactory<Album, String>("dateRange"));
		
		// Center text
		this.albumColumn.setStyle("-fx-alignment: CENTER;");
		this.countColumn.setStyle("-fx-alignment: CENTER;");
		this.dateOldColumn.setStyle("-fx-alignment: CENTER;");
		this.dateRangeColumn.setStyle("-fx-alignment: CENTER; -fx-font-size: 11px;");

		// Add the items
		this.albumTable.setItems(albumList);
	}
	
	private void showCustomAlert(String error)
	{
		// Setup the alert pop up
		error = "Invalid operation:\n\n" + error;
		
		this.errorAlert.setTitle("PhotoAlbum Error");
		this.errorAlert.setHeaderText(null);
		this.errorAlert.setContentText(error);
					
		// Show the alert
		this.errorAlert.showAndWait();
	}
	
	/**
	 * Sets the visibility of the form controls.
	 * 
	 * @param value True to show and false to hide.
	 */
	private void setVisibility(boolean value)
	{
		// Visibility
		this.subtextLabel.setVisible(value);
		this.editLabel.setVisible(value);
		this.albumTextField.setVisible(value);
		this.editImage.setVisible(value);
		this.submitButton.setVisible(value);
		this.cancelButton.setVisible(value);
		
		// Values
		this.editLabel.setText("");
		this.albumTextField.setText("");
	}
}
