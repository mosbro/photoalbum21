package view;

/**
 * Controller interface used by ScreenManager to switch screens.
 * Holds operations every controller needs.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public interface Controller 
{
	/**
	 * Initialization method which sets up everything on the screen. 
	 * 
	 * @param obj The argument being passed which will be casted.
	 */
	public void start(Object[] obj);
}
