package view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * DialogController sets a boolean value to true or false depending on what
 * the user pressed (yes or cancel).
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class DialogController implements Controller
{
	/** Button which sets the boolean value. */
	@FXML private Button confirmDelete, cancelDelete;
	
	/** Boolean value which holds what the user pressed. */
	private static boolean value;

	/** {@inheritDoc} */
	public void start(Object[] obj)
	{
		// Get the stage
		Stage stage = (Stage) obj[0];
		
		// Button events
		this.confirmDelete.setOnAction((event) -> { value = true; stage.close();});
		this.cancelDelete.setOnAction((event) -> { value = false; stage.close();});
	}
	
	/**
	 * Get the boolean value holding what the user pressed.
	 * 
	 * @return True if yes, false if cancel.
	 */
	public static boolean getValue() { return value; }
}
