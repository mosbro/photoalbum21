package view;

import app.StateManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import photoalbum.User;

/**
 * LoginController takes a username and password String to log a user
 * into the appropriate place. An admin user will go to the admin screen.
 * A normal user will log into the photo album view.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class LoginController implements Controller
{
	/** The menu bar control with all the actions. */
	@FXML private MenuController menuController;
	
	/** TextField that holds the username. */
	@FXML private TextField userTxt;
	
	/** PasswordField that holds the password. */
	@FXML private PasswordField passTxt;
	
	/** Button that logs you in depending on the text field. */
	@FXML private Button loginBtn;
	
	/** Alert dialog box to show error messages. */
	private Alert errorAlert;
	
	/** {@inheritDoc} */
	public void start(Object[] obj)
	{
		// Setup stuff
		this.errorAlert = new Alert(AlertType.ERROR);
		this.menuController.start(null);
		menuController.setLoginVisibility();
		
		// Create button events
		this.loginBtn.setOnAction((event) -> this.login(event));
	}
	
	/**
	 * Login button event that attempts to log a user in and
	 * change to the appropriate screen.
	 * 
	 * @param e ActionEvent for closing a window.
	 */
	private void login(ActionEvent e)
	{
		// Find the username typed in the text field
		String username = userTxt.getText();
		String password = passTxt.getText();
		User user = StateManager.getInstance().getUsers().findUser(username);
		
		// Check the credentials
		if(user != null && user.checkInfo(username, password))
		{
			// Load appropriate screen (admin or normal user)
			if(user.getUsername().toLowerCase().equals("admin"))
				StateManager.getInstance().loadScene("Admin.fxml", null);
			else
				StateManager.getInstance().loadScene("Album.fxml", new Object[] {user});
		}
		else
			this.showCustomAlert("Invalid username or password!");
	}
	
	/**
	 * Brings up alert dialog box to show error messages.
	 * 
	 * @param error The message to be shown.
	 */
	private void showCustomAlert(String error)
	{
		// Setup the alert pop up
		error = "Invalid operation:\n\n" + error;
		
		this.errorAlert.setTitle("PhotoAlbum Error");
		this.errorAlert.setHeaderText(null);
		this.errorAlert.setContentText(error);
					
		// Show the alert
		this.errorAlert.showAndWait();
	}
	
}
