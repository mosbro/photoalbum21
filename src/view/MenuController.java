package view;

import app.StateManager;
import javafx.fxml.FXML;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import photoalbum.User;

/**
 * MenuController has all the menu buttons. It sets the visibility for each screen
 * and sets the operations on a menu item.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class MenuController implements Controller
{
	/** The menu item under File. */
	@FXML private Menu file, edit, help;
	
	/** The menu item under Edit. */
	@FXML private MenuItem search, close, logout, exit;
	
	/** The menu item under Edit. */
	@FXML private MenuItem createAlbum, renameAlbum, deleteAlbum;
	
	/** The menu item under Edit. */
	@FXML private MenuItem addPhoto, removePhoto, editCaption, addTag, removeTags, copyMove;
	
	/** {@inheritDoc} */
	public void start(Object[] arg)
	{
		// Get the user
		User user = (arg != null) ? (User) arg[0] : null;
		
		// Menu events
		search.setOnAction(e -> StateManager.getInstance().loadScene("Search.fxml", new Object[] {user}));
		close.setOnAction(e -> StateManager.getInstance().loadScene("Album.fxml", new Object[] {user}));
		logout.setOnAction(e -> StateManager.getInstance().loadScene("Login.fxml", null));
		exit.setOnAction(e -> System.exit(0));
	}
	
	/**
	 * Disables/Enables certain menu items in the login page.
	 */
	public void setLoginVisibility() 
	{ 
		search.setDisable(true);
		close.setDisable(true);
		logout.setDisable(true);
		edit.setDisable(true);
	}
	
	/**
	 * Disables/Enables certain menu items in the admin page.
	 */
	public void setAdminVisibility()
	{
		search.setDisable(true);
		close.setDisable(true);
		logout.setDisable(false);
		edit.setDisable(true);
	}
	
	/**
	 * Disables/Enables certain menu items in the album page.
	 */
	public void setAlbumVisibility()
	{
		edit.setDisable(false);
		search.setDisable(false);
		logout.setDisable(false);
		close.setDisable(true);
		createAlbum.setDisable(false);
		renameAlbum.setDisable(false);
		deleteAlbum.setDisable(false);
		addPhoto.setDisable(true);
		removePhoto.setDisable(true);
		editCaption.setDisable(true);
		addTag.setDisable(true);
		removeTags.setDisable(true);
		copyMove.setDisable(true);
	}
	
	/**
	 * Disables/Enables certain menu items in the photo page.
	 */
	public void setPhotoVisibility()
	{
		edit.setDisable(false);
		search.setDisable(false);
		logout.setDisable(false);
		close.setDisable(false);
		createAlbum.setDisable(true);
		renameAlbum.setDisable(true);
		deleteAlbum.setDisable(true);
		addPhoto.setDisable(false);
		removePhoto.setDisable(false);
		editCaption.setDisable(false);
		addTag.setDisable(false);
		removeTags.setDisable(false);
		copyMove.setDisable(false);
	}
	
	/**
	 * Disables/Enables certain menu items in the search page.
	 */
	public void setSearchVisibility()
	{
		edit.setDisable(true);
		search.setDisable(true);
		logout.setDisable(false);
		close.setDisable(false);
	}
	
	/**
	 * Gets a menu items.
	 * 
	 * @param menuItem The menu item being searched for.
	 * @return A menu item object.
	 */
	public MenuItem getMenuItem(String menuItem)
	{
		switch(menuItem.toLowerCase())
		{
			case "createalbum":
				return createAlbum;
			case "renamealbum":
				return renameAlbum;
			case "deletealbum":
				return deleteAlbum;
			case "addphoto":
				return addPhoto;
			case "removephoto":
				return removePhoto;
			case "editcaption":
				return editCaption;
			case "addtag":
				return addTag;
			case "removetags":
				return removeTags;
			case "copymove":
				return copyMove;
		}
		
		return null;
	}
}
