package view;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import app.StateManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import photoalbum.Album;
import photoalbum.Photo;
import photoalbum.Tag;
import photoalbum.User;

/**
 * PhotoController class shows all the photos in an album by retrieving
 * the photo locations from disk and putting them in a list view.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class PhotoController implements Controller
{
	/** MenuController has all the menu items. */
	@FXML private MenuController menuController;
	
	/** Image being shown on the photo view page. */
	@FXML private ImageView bigImage, editImage, noImage;
	
	/** Photo detail label. */
	@FXML private Label photoCaptionLabel, titleLabel, photoDateLabel;
	
	/** Label showing details for a photo. */
	@FXML private Label noPhotoLabel, photoTagLabel, editLabel;
	
	/** Button that does an operation on a photo. */
	@FXML private Button copyButton, moveButton, submitButton, cancelButton;
	
	/** Photo detail held this text field. */
	@FXML private TextField captionTextField, tagTypeTextField, tagValueTextField;
	
	/** A list view of photos with their captions. */
	@FXML private ListView<Photo> listView;
	
	/** A list view of tags for a photo. */
	@FXML private ListView<Tag> tagListView, photoTagList;
	
	/** A list view of albums for a user. */
	@FXML private ListView<Album> albumListView;

	/** Alert dialog box to show error messages. */
	private Alert errorAlert;
	
	/** List of photos for an album. */
	private ObservableList<Photo> imageList;
	
	/** List of tags for a photo. */
	private ObservableList<Tag> tagList;
	
	/** List of albums for a user. */
	private ObservableList<Album> albumList;
	
	/** The current user holding all the information. */
	private User user;
	
	/** Index of the album in the list of albums for the user. */
	private int albumIndex;
	
	/** Boolean values to know what is being done. */
	private boolean isEditCaption, isAddTag, isDeleteTag;
	
	/** {@inheritDoc} */
	public void start(Object[] obj) 
	{
		// Necessary operations
		this.user = (User) obj[0];									// Get the user object
		this.albumIndex = (int) obj[1];								// The index of the album
		this.imageList = FXCollections.observableArrayList();		// Create new image list
		this.tagList = FXCollections.observableArrayList();			// Create new tag list
		this.albumList = FXCollections.observableArrayList();		// Create new album list
		this.errorAlert = new Alert(AlertType.ERROR);				// Alert dialog box for error messages
		this.menuController.start(new Object[] {this.user});		// Setup the menu
		this.menuController.setPhotoVisibility();					// Disable certain menu elements
		
		// Set boolean values
		this.isEditCaption = this.isAddTag = this.isDeleteTag = false;
		
		// Set items for album list view
		for(int i = 0; i < user.getAlbumListSize(); i++)
		{
			if(!user.getAlbum(i).getAlbumName().equals(this.user.getAlbum(this.albumIndex).getAlbumName()))
				this.albumList.add(user.getAlbum(i));
		}
		this.albumListView.setItems(albumList);
		
		// Setup photo list view
		this.setupListView();
		
		// Check if any photos and show label
		if(this.imageList.size() == 0)
			this.noPhotoLabel.setVisible(true);
		else
		{
			this.listView.getSelectionModel().select(0);
			this.listViewPhotos();
		}
		
		// Menu events
		this.menuController.getMenuItem("AddPhoto").setOnAction(e -> this.addPhoto());
		this.menuController.getMenuItem("RemovePhoto").setOnAction(e -> this.removePhoto());
		this.menuController.getMenuItem("EditCaption").setOnAction(e -> this.editCaption());
		this.menuController.getMenuItem("AddTag").setOnAction(e -> this.addTag());
		this.menuController.getMenuItem("RemoveTags").setOnAction(e -> this.removeTags());
		this.menuController.getMenuItem("CopyMove").setOnAction(e -> this.copyMove());
		
		// Button events
		this.copyButton.setOnAction(e -> this.copyPhoto());
		this.moveButton.setOnAction(e -> this.movePhoto());
		this.submitButton.setOnAction(e -> this.submit());
		this.cancelButton.setOnAction(e -> this.cancel());
		
		// Listener events
		this.listView.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) -> this.listViewPhotos());
	}
	
	/**
	 * Add a photo the album and list view.
	 */
	private void addPhoto()
	{
		// Open file browser and set to only look for images
		FileChooser fileChooser = new FileChooser();
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image files (*.png, *.jpg, *.jpeg, *.gif", "*.png", "*.jpg", "*.jpeg", "*.gif");
		fileChooser.getExtensionFilters().add(extFilter);
		File file = fileChooser.showOpenDialog(StateManager.getInstance().getStage());
		
		// Check if file is good
		if(file != null)
		{
			// Add new photo with file path
			Photo photo = new Photo("file:" + file.getAbsolutePath());
			photo.setDate();			
			
			// Add to user albums and listview
			user.getAlbum(this.albumIndex).addPhoto(photo);
			imageList.add(photo);
			
			// Select the photo
			listView.getSelectionModel().select(this.imageList.size() - 1);
			
			// Serialize the user
			this.updateUser();
		}
		
		// Check size of list
		if(this.imageList.size() != 0)
		{
			this.noPhotoLabel.setVisible(false);
			this.photoCaptionLabel.setVisible(true);
			this.photoDateLabel.setVisible(true);
			this.photoTagLabel.setVisible(true);
			this.setFormsVisibility(false);
		}
	}
	
	/**
	 * Remove a photo from the album and the list view.
	 */
	private void removePhoto()
	{
		// Only show if photos in list
		if(this.imageList.size() != 0)
		{
			// Hide forms
			this.setFormsVisibility(false);;
			
			// Get selected index
			int index = this.listView.getSelectionModel().getSelectedIndex();
			
			// Show confirmation dialog
			this.showDeleteDialog();
			
			// Check what the user clicked and remove photo
			if(DialogController.getValue() == true)
			{
				user.getAlbum(this.albumIndex).removePhoto(index);
				imageList.remove(index);
				
				// Update user
				this.updateUser();
				
				// Select a photo
				listView.getSelectionModel().select(index);
			}
		}
		else
			this.showCustomAlert("No photos available!");
	}
	
	/**
	 * Add/Rename a caption on a photo.
	 */
	private void editCaption()
	{
		// Only show if photos in list
		if(this.imageList.size() != 0)
		{
			// Set to editing caption
			this.isEditCaption = true;
			this.isAddTag = this.isDeleteTag = false;
			
			// Set form visibility
			this.setFormsVisibility(false);
			this.editLabel.setVisible(true);
			this.editImage.setVisible(true);
			this.titleLabel.setVisible(true);
			this.submitButton.setVisible(true);
			this.cancelButton.setVisible(true);
			this.captionTextField.setVisible(true);
			
			// Set text
			this.titleLabel.setText("Please enter a new caption:");
			this.editLabel.setText("Caption");
			
			// Request focus
			this.captionTextField.requestFocus();
		}
		else
			this.showCustomAlert("No photos available!");
	}
	
	/**
	 * Add a tag (tag type and type value) to a photo.tagTypeTextValue
	 */
	private void addTag()
	{
		// Only show if photos in list
		if(this.imageList.size() != 0)
		{
			// Set to editing caption
			this.isAddTag = true;
			this.isEditCaption = this.isDeleteTag = false;
			
			// Set form visibility
			this.setFormsVisibility(false);
			this.editLabel.setVisible(true);
			this.editImage.setVisible(true);
			this.titleLabel.setVisible(true);
			this.submitButton.setVisible(true);
			this.cancelButton.setVisible(true);
			this.tagTypeTextField.setVisible(true);
			this.tagValueTextField.setVisible(true);
			
			// Set text
			this.titleLabel.setText("Please enter tag type & value:");
			this.editLabel.setText("Tag");
			
			// Request focus
			this.tagTypeTextField.requestFocus();
		}
		else
			this.showCustomAlert("No photos available!");
	}
	
	/**
	 * Remove a tag from a photo.
	 */
	private void removeTags()
	{
		// Only show if photos in list
		if(this.imageList.size() != 0)
		{
			// Set to editing caption
			this.isDeleteTag = true;
			this.isEditCaption = this.isAddTag = false;
			
			// Set form visibility
			this.setFormsVisibility(false);
			this.editLabel.setVisible(true);
			this.editImage.setVisible(true);
			this.titleLabel.setVisible(true);
			this.submitButton.setVisible(true);
			this.cancelButton.setVisible(true);
			this.tagListView.setVisible(true);
			this.photoTagList.setVisible(false);
			
			// Set text
			this.titleLabel.setText("Choose a tag to delete:");
			this.editLabel.setText("Tags");
			this.submitButton.setText("Delete");
			this.photoTagLabel.setText("Tags: ----->");
			
			// Request focus
			this.tagListView.requestFocus();
			
			// Populate the list view
			this.updateTags();
		}
		else
			this.showCustomAlert("No photos available!");
	}
	
	/**
	 * Copy or move a photo from an album to another album.
	 */
	private void copyMove()
	{
		// Only show if photos in list
		if(this.imageList.size() != 0)
		{
			// Set to editing caption
			this.isEditCaption = this.isDeleteTag = this.isAddTag = false;
			
			// Set form visibility
			this.setFormsVisibility(false);
			this.editImage.setVisible(true);
			this.titleLabel.setVisible(true);
			this.copyButton.setVisible(true);
			this.moveButton.setVisible(true);
			this.cancelButton.setVisible(true);
			this.albumListView.setVisible(true);
			
			// Set text
			this.titleLabel.setText("Please choose an album:");
			
			// Request focus
			this.albumListView.requestFocus();
		}
		else
			this.showCustomAlert("No photos available!");
	}
	
	/**
	 * Copy a photo to another album.
	 */
	private void copyPhoto()
	{
		// Only do if albums in list
		if(this.albumList.size() != 0)
		{
			// Get selected album and photo
			String albumName = this.albumListView.getSelectionModel().getSelectedItem().getAlbumName();
			Photo photo = this.listView.getSelectionModel().getSelectedItem();
			
			// Copy photo over
			int aIndex = this.user.findAlbumIndex(albumName);
			user.getAlbum(aIndex).addPhoto(photo);
			
			// Update user
			this.updateUser();
			
			// Form visibility
			this.setFormsVisibility(false);
		}
		else
			this.showCustomAlert("No albums available!");
	}
	
	/**
	 * Move a photo to another album.
	 */
	private void movePhoto()
	{
		// Only do if albums in list
		if(this.albumList.size() != 0)
		{
			// Get selected album and photo
			String albumName = this.albumListView.getSelectionModel().getSelectedItem().getAlbumName();
			Photo photo = this.listView.getSelectionModel().getSelectedItem();
			
			// Move photo over
			int aIndex = this.user.findAlbumIndex(albumName);
			int pIndex = this.listView.getSelectionModel().getSelectedIndex();
			this.user.getAlbum(aIndex).addPhoto(photo);
			this.user.getAlbum(this.albumIndex).removePhoto(pIndex);
			this.imageList.remove(pIndex);
			
			// Update user
			this.updateUser();
			
			// Form visibility
			this.setFormsVisibility(false);
			
			// Select a photo
			listView.getSelectionModel().select(pIndex);
		}
		else
			this.showCustomAlert("No albums available!");
	}
	
	/**
	 * Apply the current operation being done.
	 */
	private void submit()
	{
		// Get selected index
		int index = this.listView.getSelectionModel().getSelectedIndex();
		
		// Boolean to see if operation was valid
		boolean isValid = false;
		
		// CAPTION
		if(this.isEditCaption)
		{
			// Get text
			String caption = "   " + this.captionTextField.getText();
			
			if(!caption.equals("   "))
			{
				user.getAlbum(this.albumIndex).getPhoto(index).setCaption(caption);
				imageList.set(index, user.getAlbum(this.albumIndex).getPhoto(index));
				isValid = true;
			}
			else
				this.showCustomAlert("Caption cannot be empty!");
		}
		
		// ADD TAGS
		else if(this.isAddTag)
		{
			// Get tag type and value
			String tagType = this.tagTypeTextField.getText();
			String tagValue = this.tagValueTextField.getText();
			
			if(!tagType.equals("") && !tagValue.equals(""))
			{
				// Create new tag
				Tag tag = new Tag(tagType, tagValue);
				
				// Try to add tags
				if(user.getAlbum(this.albumIndex).getPhoto(index).addTag(tag))
				{
					imageList.set(index, user.getAlbum(this.albumIndex).getPhoto(index));
					isValid = true;
				}
				else
					this.showCustomAlert("Tag already exists!");
			}
			else
				this.showCustomAlert("Please fill out both tag fields!");
		}
		
		// REMOVE TAGS
		else if(this.isDeleteTag)
		{
			// Get selected index of tags
			int tagIndex = this.tagListView.getSelectionModel().getSelectedIndex();
			
			if(tagList.size() != 0)
			{
				// Show dialog confirmation
				this.showDeleteDialog();
				
				if(DialogController.getValue() == true)
				{
					this.user.getAlbum(this.albumIndex).getPhoto(index).removeTag(tagIndex);
					this.tagList.remove(tagIndex);
					isValid = true;
				}
			}
			else
				this.showCustomAlert("No tags available!");
		}
		
		// SUCCESS
		if(isValid)
		{
			// Update the user
			this.updateUser();
			
			// Hide forms
			this.setFormsVisibility(false);
			
			if(this.isDeleteTag)
				this.removeTags();
			
			// Update listview
			this.listViewPhotos();
			
			// Select a photo
			listView.getSelectionModel().select(index);
		}
	}
	
	/**
	 * Cancel the current operation being done.
	 */
	private void cancel()
	{
		// Hide everything
		this.setFormsVisibility(false);
		
		this.photoTagList.setVisible(true);
		this.photoTagLabel.setText("Tags: ");
	}
	
	/**
	 * Listview listener for the photos.
	 */
	private void listViewPhotos()
	{
		// Get selected index
		int index = this.listView.getSelectionModel().getSelectedIndex();
		
		if(index >= 0)
		{
			// Visibility of big picture
			this.noImage.setVisible(false);
			this.bigImage.setVisible(true);
			this.setDetailsVisibility(true);
			
			// Show image
			this.bigImage.setImage(new Image(this.user.getAlbum(this.albumIndex).getPhoto(index).getLocation()));
			
			// Show details
			String caption = this.user.getAlbum(this.albumIndex).getPhoto(index).getCaption().substring(3);
			int month = this.user.getAlbum(this.albumIndex).getPhoto(index).getDate().get(Calendar.MONTH);
			int day = this.user.getAlbum(this.albumIndex).getPhoto(index).getDate().get(Calendar.DAY_OF_MONTH);
			int year = this.user.getAlbum(this.albumIndex).getPhoto(index).getDate().get(Calendar.YEAR);
			
			this.photoCaptionLabel.setText(caption);
			this.photoDateLabel.setText("Date: " + month + "/" + day + "/" + year);

			// Populate the tag list view
			this.updateTags();
			
			if(this.tagList.size() == 0)
			{
				this.photoTagList.setVisible(false);
				this.photoTagLabel.setText("Tags: -");
			}
			else
			{
				this.photoTagList.setVisible(true);
				this.photoTagLabel.setText("Tags:");
			}
		}
		
		if(this.imageList.size() == 0)
		{
			this.bigImage.setVisible(false);
			this.noImage.setVisible(true);
			this.setDetailsVisibility(false);
			this.noPhotoLabel.setVisible(true);
		}
	}
	
	/**
	 * Setup the image list view with captions along with a listener for new photos.
	 */
	private void setupListView()
	{
		// Add photos to image list
		for(int i = 0; i < user.getAlbum(this.albumIndex).getPhotoListSize(); i++)
			imageList.add(user.getAlbum(this.albumIndex).getPhoto(i));
		
		// Populate list view with photos
		listView.setItems(imageList);
		
		// Make list view hold images (looks through listView)
		listView.setCellFactory(param -> new ListCell<Photo>() {
			private ImageView imageView = new ImageView();
			
			@Override
			public void updateItem(Photo photo, boolean empty)
			{
				super.updateItem(photo, empty);
				
				// Check if photo is null or not
				if(empty || photo == null)
				{
					setText(null);
					setGraphic(null);
				}
				else
				{
					// Set the image properties
					imageView.setImage(new Image(photo.getLocation()));
					imageView.setFitHeight(80);
					imageView.setFitWidth(80);
					imageView.setPreserveRatio(true);
					
					// Show inside list view
					setText(photo.getCaption());
					setGraphic(imageView);
				}
			}
		});
	}
	
	/**
	 * Brings up a confirmation dialog box.
	 */
	private void showDeleteDialog()
	{
		try
		{
			// Load the dialog scene
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Dialog.fxml"));
			
			// Setup new stage
			Stage newStage = new Stage();
			newStage.setScene(new Scene((AnchorPane) loader.load()));
			newStage.setResizable(false);
			
			// Load the controller and start it
			((Controller) loader.getController()).start(new Object[] {newStage});
			
			// Show the stage and wait until it's done
			newStage.showAndWait();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update the tags in the listviews.
	 */
	private void updateTags()
	{
		// Get selected photo
		int index = this.listView.getSelectionModel().getSelectedIndex();
		this.tagList.clear();
		
		for(int i = 0; i < user.getAlbum(this.albumIndex).getPhoto(index).getTagListSize(); i++)
		{
			this.tagList.add(user.getAlbum(this.albumIndex).getPhoto(index).getTag(i));
		}
		this.tagListView.setItems(tagList);
		this.photoTagList.setItems(tagList);
	}
	
	/** 
	 * Serialize the user and update the master user list.
	 */
	private void updateUser()
	{
		user.serialize();		
		StateManager.getInstance().getUsers().setUser(user);
	}
	
	/**
	 * Show a message in the alert dialog box.
	 * 
	 * @param error The message to be shown.
	 */
	private void showCustomAlert(String error)
	{
		// Setup the alert pop up
		error = "Invalid operation:\n\n" + error;
		
		this.errorAlert.setTitle("PhotoAlbum Error");
		this.errorAlert.setHeaderText(null);
		this.errorAlert.setContentText(error);
					
		// Show the alert
		this.errorAlert.showAndWait();
	}
	
	/**
	 * Sets the photo details visibility.
	 * 
	 * @param value True to show, false to hide.
	 */
	private void setDetailsVisibility(boolean value)
	{
		this.noPhotoLabel.setVisible(false);
		this.photoCaptionLabel.setVisible(value);
		this.photoDateLabel.setVisible(value);
		this.photoTagLabel.setVisible(value);
		this.photoTagList.setVisible(value);
		
		this.photoTagLabel.setText("Tags: ");
	}
	
	/**
	 * Set all the forms visibility.
	 * 
	 * @param value True to show, false to hide.
	 */
	private void setFormsVisibility(boolean value)
	{
		this.editLabel.setVisible(value);
		this.titleLabel.setVisible(value);
		this.copyButton.setVisible(value);
		this.moveButton.setVisible(value);
		this.submitButton.setVisible(value);
		this.cancelButton.setVisible(value);
		this.tagListView.setVisible(false);
		this.albumListView.setVisible(value);
		this.editImage.setVisible(false);
		this.captionTextField.setVisible(value);
		this.tagTypeTextField.setVisible(value);
		this.tagValueTextField.setVisible(value);
		
		this.tagTypeTextField.setText("");
		this.tagValueTextField.setText("");
		this.submitButton.setText("Submit");
	}
}
