package view;

import java.util.Calendar;
import java.util.Locale;
import java.util.Optional;

import app.StateManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import photoalbum.Album;
import photoalbum.Photo;
import photoalbum.Tag;
import photoalbum.User;

/**
 * SearchController is able to search by tag type and value as well
 * as the date range of photos.
 * 
 * @author Mohammad Salim (mfs134)
 * @author Lukasz Gremza (ldg65)
 */
public class SearchController implements Controller 
{
	/** The menu bar control with all the actions. */
	@FXML private MenuController menuController;
	
	/** Button that performs operation on the text field value. */
	@FXML private Button searchByTagButton, searchByDateButton, cancelButton, newAlbumButton;
	
	/** List which displays all the photos after searching. */
	@FXML private ListView<Photo> searchListView;
	
	/** TextField that holds the search type and value. */
	@FXML private TextField searchType, searchValue;

	/** DatePicker that holds the dates. */
	@FXML private DatePicker fromDate, toDate;
	
	/** List of photos for the list view. */
	private ObservableList<Photo> imageList;
	
	/** Current user that holds all the albums and photos. */
	private User user;
	
	/** {@inheritDoc} */
	public void start(Object[] obj) 
	{
		// Set the locale
		Locale.setDefault(Locale.US);
		
		// Get the user
		this.user =  StateManager.getInstance().getUsers().findUser(((User) obj[0]).getUsername());
		
		// Necessary operations
		this.imageList = FXCollections.observableArrayList();		// Create new image list
		this.menuController.start(new Object[] {user});				// Setup menu
		this.menuController.setSearchVisibility();					// Enable/Disable menu items
		
		// Button events
		this.newAlbumButton.setOnAction(e -> createNewAlbum());
		this.cancelButton.setOnAction(e -> this.clear());
		this.searchByTagButton.setOnAction(e -> 
		{
			int index = tagSearch();
			if(index > 0)
				newAlbumButton.setVisible(true);
		});
		
		this.searchByDateButton.setOnAction(e -> 
		{
			int index = dateSearch();
			if(index > 0)
				newAlbumButton.setVisible(true);
		});
	}
	
	/**
	 * Creates a new album from the search results.
	 */
	private void createNewAlbum()
	{
		Album temp;
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("");
		dialog.setHeaderText("Create New Album");
		dialog.setContentText("Please enter a name:");
		Optional<String> result = dialog.showAndWait();
		
		if (result.isPresent())
		{
			String name = result.get();
			temp = new Album(name);
			for(int i = 0; i < imageList.size();i++)
				temp.addPhoto(imageList.get(i));
			user.addAlbum(temp);
			user.serialize();
			StateManager.getInstance().getUsers().setUser(user);
		}
	}
	
	/**
	 * Populates the list view with photos from the search results.
	 */
	private void populate()
	{
		
		// Populate list view with photos
		searchListView.setItems(imageList);
		
		// Make list view hold images (looks through listView)
		searchListView.setCellFactory(param -> new ListCell<Photo>() {
			private ImageView imageView = new ImageView();
			
			@Override
			public void updateItem(Photo photo, boolean empty)
			{
				super.updateItem(photo, empty);
				
				// Check if photo is null or not
				if(empty || photo == null)
				{
					setText(null);
					setGraphic(null);
				}
				else
				{
					// Set the image properties
					imageView.setImage(new Image(photo.getLocation()));
					imageView.setFitHeight(80);
					imageView.setFitWidth(80);
					imageView.setPreserveRatio(true);
					
					// Show inside list view
					setText(photo.getCaption());
					setGraphic(imageView);
				}
			}
		});
	}
	
	/**
	 * Clear the listview.
	 */
	private void clear()
	{
		searchListView.getItems().clear();
	}
	
	/**
	 * Searches by the tag type and tag value.
	 * 
	 * @return An integer with the size of the list.
	 */
	private int tagSearch()
	{
		imageList.clear();
		
		// Add photos to image list
		for(int i = 0; i < user.getAlbumListSize(); i++)
		{
			for(int j = 0; j < user.getAlbum(i).getPhotoListSize(); j++)
			{
				if(user.getAlbum(i).getPhoto(j).checkTag(new Tag(searchType.getText(), searchValue.getText())) != false)
					imageList.add(user.getAlbum(i).getPhoto(j));
			}
		}
		
		this.populate();
		return imageList.size();
	}
	
	/**
	 * Searches by the dates given.
	 * 
	 * @return An integer with the size of the list.
	 */
	private int dateSearch()
	{
		imageList.clear();
		
		if(this.fromDate.getValue() != null && this.toDate.getValue() != null)
		{
			Calendar fromDateObj = Calendar.getInstance();
			Calendar toDateObj = Calendar.getInstance();
			
			fromDateObj.set(fromDate.getValue().getYear(), fromDate.getValue().getMonthValue(), fromDate.getValue().getDayOfMonth());
			toDateObj.set(toDate.getValue().getYear(), toDate.getValue().getMonthValue(), toDate.getValue().getDayOfMonth());
			
			// Add photos to image list
			for(int i = 0; i < user.getAlbumListSize(); i++)
			{
				for(int j = 0; j < user.getAlbum(i).getPhotoListSize(); j++)
				{
					if(user.getAlbum(i).getPhoto(j).isInRange(fromDateObj, toDateObj) != false)
						imageList.add(user.getAlbum(i).getPhoto(j));
				}
			}
		}
		this.populate();
		return imageList.size();
	}
}
